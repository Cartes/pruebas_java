import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.InvocationTargetException;

import javax.swing.*;
import Principal.Cajas;
import Principal.Mario;

enum formas {
	Quieto,
	Agachado,
	Salto_Iz,
	Salto_Der,
	Izquierda1,
	Izquierda2,
	Izquierda3,
	Derecha1,
	Derecha2,
	Derecha3
}
/*
enum objetos {
	Nada,
	Moneda,
	SetaG,
	SetaF,
	Estrella,
	LevelUp
}
*/
@SuppressWarnings("serial")
public class Mario_Bros extends JPanel {

	// JFrame
	private static JFrame marco;
	// Dimensiones de la ventana (JFrame)
	public final static int ANCHO = 1200;
	public final static int ALTO = 600;
    // Bloques
	public static int ANCHO_bloques = 48;
	public static int ALTO_bloques = 50;
	// suelo
    public int suelo = ALTO_bloques * 9 + 20;
    // Personaje (Mario)
	Mario mario = new Mario (0, 0, suelo);    
    // Posicion de Mario
    public double x, y;
    public int forma;
    
    // Velocidad de movimiento
    protected double velocidad = 0.15;
    
    // Booleans para botones    
    // Menu
    public boolean entrar, salir;
    public int menu;
    // Juego
    public boolean salto, caida, agacharse, derecha, izquierda;
    
    // Fondo
    public float ANCHO_fondo = ANCHO;
    public int ALTO_fondo = ALTO + 120;
    public float xGame = 0;
    public float yGame = 0;
		// Posicion del fondo
    public float x_fondo = 0;
		// Final del fondo
    public int x_final = 9500;
    // Fin de la partida
    public boolean end = false;
    public boolean empieza = false;
	// Posiciones y obstaculos
	Cajas[] cajas = {
			new Cajas("cajaSorpresa", 11, 3),
			new Cajas("cajaSorpresa", 12, 3),
			new Cajas("cajaSorpresa", 13, 3),
			new Cajas("cajaSorpresa", 15, 3),
			new Cajas("obstaculo", 8, 0),
			new Cajas("obstaculo", 9, 0),
			new Cajas("obstaculo", 9, 1)
			};
	// Posicion de las Setas
	private int[][] posSeta = new int[][] {
		{25, 0},
	};
	// Flecha del menu
	private int[][] posFlecha = new int[][] {
		{350, ALTO/2 - 35},
		{450, ALTO/2 - 35 + 50},
		{450, ALTO/2 - 35 + 100}
	};
	private int flecha_num = 999;
	// Imagenes
	private Image fondo;
	private Image mario_foto;
	private Image seta;
	private Image imagen_menu;
	private Image flecha;
	// Monedas
	private int monedas = 0;
	// Para la animacion de moverse
	private int animacion = 0;
	private int animacion1 = 0;

	boolean hasChocado;
	boolean cambioCaja;
    public Mario_Bros() {
    	menu = 1;
    	// Las Primeras coordenadas de Mario
        x = 0;
        y = mario.getSuelo_Mario();
        // Cojiendo las imagenes
    	fondo = new ImageIcon("src/Media/fondo-Mario.png").getImage();
    	mario_foto = new ImageIcon("src/Media/sprite_Mario3.png").getImage();
    	seta = new ImageIcon("src/Media/seta.png").getImage();
    	imagen_menu = new ImageIcon("src/Media/imagen-menu.png").getImage();
    	flecha = new ImageIcon("src/Media/flecha.png").getImage();
    	// Crea unas dimensiones de donde se va a actuar
    	// El JFrame es todo pero se pone todo en el JPanel
        setPreferredSize(new Dimension(ANCHO, ALTO));
        setFocusable(true);

    	addKeyListener(new KeyAdapter() {
        		// Hay que poner como minimo estos dos eventos
        		// Pressed actua cuando se presiona 
        		// Released actua cuando se deja de presionar
                public void keyPressed(KeyEvent e) {
                    actualiza(e.getKeyCode(), true);
                }
                
                public void keyReleased(KeyEvent e) {
                    actualiza(e.getKeyCode(), false);
                    animacion = 0;
                    animacion1 = 0;
                }

                private void actualiza(int keyCode, boolean pressed) {
                	// Botones menu
                	if(!empieza) {
                		switch (keyCode) {
                        	case KeyEvent.VK_UP:
                        	case KeyEvent.VK_W:
                        		if (pressed)
                        			flecha_num--;
                        		break;
                        	case KeyEvent.VK_DOWN:
                        	case KeyEvent.VK_S:
                        		if (pressed)
                        			flecha_num++;
                            	break;
                        	case KeyEvent.VK_ENTER:
                        		entrar = true;
                        		break;
                        	case KeyEvent.VK_ESCAPE:
                        		salir = true;
                        		break;
                    	}
                	}
                	// Botones juego
                	else {
                		switch (keyCode) {
                    		case KeyEvent.VK_UP:
                    		case KeyEvent.VK_W:
                    		case KeyEvent.VK_SPACE:
                    			if (!caida)
                    				salto = true;
                    			break;
                    		case KeyEvent.VK_DOWN:
                    		case KeyEvent.VK_S:
                    			agacharse = pressed;
                    			break;
                    		case KeyEvent.VK_LEFT:
                    		case KeyEvent.VK_A:
                    			izquierda = pressed;
                    			break;
                    		case KeyEvent.VK_RIGHT:
                    		case KeyEvent.VK_D:
                    			derecha = pressed;
                    			break;
                		}
                	}
                }
            });
    }

    public static void main(String[] args) {
    	
    	marco = new JFrame("Mario Bros");
        // NO hace falta pero esta bien ponerlo
    	marco.addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent e) {
    			System.exit(0);
    		}
    	});
    	marco.setResizable(false);
    	marco.setEnabled(true);
    	marco.setSize(new Dimension(ANCHO, ALTO));
    	marco.setLocationRelativeTo(null);
        Mario_Bros comienzo = new Mario_Bros();
        marco.getContentPane().add(comienzo);
        marco.pack();
        marco.setVisible(true);
        marco.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
        comienzo.cicloJuego();
    }
    
    // Funciones Principales
    public void paint(Graphics g) {
    	
    	if (!empieza) {
    		if (menu == 1)
    			menuP (g);
    		/*
    		else if (menu == 2)
    			menuControles (g);
    		else
    			menuCreditos (g);*/
    	}
    	else
    		juego (g);
    }
    
    public void menuP (Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, ANCHO, ALTO);
		//g.drawImage(imagen_menu, 0, 0, 1200, 600, null);
		
		// Titulo
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		g.drawString("SUPER MARIO BROS", 400, 130);
		// Flecha
		g.drawImage(flecha, posFlecha[flecha_num % posFlecha.length][0], posFlecha[flecha_num % posFlecha.length][1], 70, 50, null);
		// Empezar
		g.setFont(new Font("TimesRoman", Font.BOLD, 27));
		g.setColor(Color.WHITE);
		g.drawString("Pulsar ENTER para empezar", 450, ALTO/2);
		// Creditos
		g.setColor(Color.CYAN);
		g.drawString("Controles", 550, ALTO/2 + 50);
		// Creditos
		g.setColor(Color.GREEN);
		g.drawString("Creditos", 560, ALTO/2 + 100);
		// Musica
		g.setFont(new Font("TimesRoman", Font.BOLD, 25));
		g.setColor(Color.RED);
		g.drawString("Pulse O para habilitar musica", 0, 25);
	}
    
    public void juego (Graphics g) {
		// Fondo
		g.drawImage(fondo, 0, 0, ANCHO, ALTO, (int) x_fondo, 0, (int) ANCHO_fondo, ALTO_fondo, null);
    	// System.out.println(x_fondo);
    
    	// Monedas
    	g.setFont(new Font("TimesRoman", Font.BOLD, 27));
    	g.setColor(Color.WHITE);
    	g.drawString("Coins: " + monedas, 40, 40);
    
    	g.setColor(Color.BLACK);
    	g.fillRect( (int) x, (int) y, mario.getAnchoMario(), mario.getAltoMario());

		// Seta
		dibujaBloques(g, seta, ANCHO_bloques * posSeta[0][0], suelo, ANCHO_bloques, ALTO_bloques);
		// Caja
		// xGame yGame por defecto son 0
		// Queremos pintar una caja en xGame 250
		// ANCHO ES NUESTRO MAXIMO
		// Lee todas las posiciones de las cajas y las pinta
		for (int i = 0; i < cajas.length; i++)
			dibujaBloques(g,
					cajas[i].getImagen(),
					cajas[i].getPosX(), cajas[i].getPosY(),
					ANCHO_bloques, ALTO_bloques);
    	// Marios
		g.drawImage(mario_foto, 100, 100, 150, 150,
				mario.forma_Mario[forma][0], mario.forma_Mario[forma][1], mario.forma_Mario[forma][2], mario.forma_Mario[forma][3],
				null);
		g.drawImage(mario_foto, (int) x, (int) mario.getSuelo_Mario(), (int) x + mario.getAnchoMario(), (int) y + mario.getAltoMario(),
				mario.forma_Mario[forma][0], mario.forma_Mario[forma][1], mario.forma_Mario[forma][2], mario.forma_Mario[forma][3],
				null);
	}
    
    public void dibujaBloques(Graphics g, Image img, int xObject, int yObject, int width, int height) {
    	double xMin = (xGame - mario.getX());
    	double xMax = xMin + ANCHO;
    	if ( xObject + width >= xMin && xObject <= xMax )
    		g.drawImage(img, (int) (xObject - xGame + x), (int) (yObject - yGame), width, height, null);
    }
    
    private void dibuja() {
        try {
			SwingUtilities.invokeAndWait(new Runnable() {
			        public void run() {
			        	repaint();
			        	// paintImmediately(0, 0, ANCHO, ALTO);
			        }
			    });
		} catch (InvocationTargetException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    // Ciclo principal del juego
    public void cicloJuego() {
    	while (!empieza) {
            dibuja();
            if (entrar) {
            	if (flecha_num % posFlecha.length == 0)
            		empieza = true;
            	else if (flecha_num % posFlecha.length == 1) {
            		// Controles
            		menu = 2;
            		entrar = false;
            	}
            	else {
            		// Creditos
            		menu = 3;
        			entrar = false;
        		}
            }
            if (salir)
            	// menu principal
            	menu = 1;
    	}
        while (!end) {
        	movimientos();
        	formas();
            cojeSeta();
            
            dibuja();
            if (xGame >= x_final)
            	end = true;
            //System.out.println(xGame + " - " + y + "\nx = " + Math.round(xGame / ANCHO_bloques) + " - y = " + (9 - Math.floor(y / ALTO_bloques)));
            //System.out.println(longitudSalto);
        }
        animacionFinal();
    }
    // Otras Funciones 
    // Funcion con los movimientos
    private void movimientos() {
    	// Mov. ir a la Izquierda
        if (izquierda ) {//&& !choque()) {
        	if (xGame > 0)
        		xGame -= velocidad;
        	if (xGame > 400) {
        		x_fondo -= velocidad;
        		ANCHO_fondo -= velocidad;
        	}
        	else if (x > 0)
        		x -= velocidad;
        }
        
        // Mov. ir a la derecha
        if (derecha ) {//&& !choque()) {
        	xGame += velocidad;
        	if (xGame > 400) {
        		x_fondo += velocidad;
        		ANCHO_fondo += velocidad;
        	}
        	else
        		x += velocidad;
        }
        
        // Mov. salto
        if (salto ) {//&& !choque()) {
        	y -= velocidad; 
        	caida = false;
        	if (y < mario.getSuelo_Mario() - mario.getLongitudSalto() ) {
        		salto = false;
        		caida = true;
        		mario.setLongitudSalto( mario.getAltoMario() * 3 );
        	}
        }
        
        // Mov. caida
        if (caida)
        	caidaFun();
        else if (!salto)
        	verSuelo();
    }
    
    public void caidaFun() {
    	boolean sigoCayendo = true;
    	for (int i = 0; i < cajas.length && caida; i++) {
    		if (Math.round(xGame / ANCHO_bloques) == cajas[i].getPosX() / ANCHO_bloques &&
    				Math.round(y / mario.getAltoMario()) > 9 - cajas[i].getPosY() / mario.getAltoMario())
    		{
    			mario.setSuelo_Mario( suelo - (9 - cajas[i].getPosY() / mario.getAltoMario() + 1) * mario.getAltoMario() );
    		}
    	}
    	if (sigoCayendo && y < mario.getSuelo_Mario())
    		y += velocidad;
    	else
    		caida = false;
    }
    
    public void verSuelo() {
    	boolean haySuelo = false;
    	for (int i = 0; i < cajas.length; i++) {
    		if (Math.round(xGame / ANCHO_bloques) == cajas[i].getPosX() / ANCHO_bloques &&
    				9 - mario.getSuelo_Mario() / mario.getAltoMario() - 1 == cajas[i].getPosY() ) {
    			haySuelo = true;
    			break;
    		}
    	}
    	if (!haySuelo) {
    		mario.setSuelo_Mario(suelo);
    		caida = true;
    		caidaFun();
    	}
    }
    
    public void cojeSeta() {
    	if (Math.floor(xGame / ANCHO_bloques) == posSeta[0][0] &&
    			9 - Math.round(y / ALTO_bloques) == posSeta[0][1]) {
    		 posSeta[0][0] = -10;
    		 cambioTamanio("Grande");
    	}
    }
    
    public void cambioTamanio(String tamanio) {
    	if (tamanio == "Grande") {
    		mario.setSuelo_Mario(mario.getSuelo_Mario() - 50);
    	}
    		
    }
    
    // Mira el si Mario se choca con algun bloque o no
    public boolean choque() {
    	hasChocado = false;
    	
    	for (int i = 0; i < cajas.length; i++) {
    		if ( derecha &&
    				Math.floor(xGame / ANCHO_bloques) + 1 == cajas[i].getPosX() / ANCHO_bloques &&
    				9 - Math.floor(y / ALTO_bloques) == ( suelo - cajas[i].getPosY() ) / ALTO_bloques )
    		{
    			hasChocado = true;
    		}
    		if ( izquierda &&
    				Math.ceil(xGame / ANCHO_bloques) - 1 == cajas[i].getPosX() / ANCHO_bloques &&
    				9 - Math.floor(y / ALTO_bloques) == ( suelo - cajas[i].getPosY() ) / ALTO_bloques )
    		{
    			hasChocado = true;
    		}
    		if ( salto &&
    				Math.round(xGame / ANCHO_bloques) == cajas[i].getPosX() / ANCHO_bloques &&
    				9 - cajas[i].getPosY() / mario.getAltoMario() > 9 - mario.getSuelo_Mario() / mario.getAltoMario() &&
    				9 - (mario.getSuelo_Mario() / mario.getAltoMario() - mario.getLongitudSalto() / mario.getAltoMario()) >= 9 - cajas[i].getPosY() / mario.getAltoMario() )
    		{
    			if (mario.getLongitudSalto() > mario.getAltoMario())
    				mario.setLongitudSalto(mario.getLongitudSalto() - mario.getAltoMario());
    			hasChocado = true;
    			cambioCaja = true;
    			//miraCaja(i);
    		}
    	}
    	
    	return hasChocado;
    }
    
    // Mira lo que hay en las cajas Sorpresa
    public void miraCaja(int caja) {
    	for (int i = 0; i < cajas.length; i++) {
    		if (cambioCaja &&
    				caida &&
    				cajas[i].getTipoCaja() == 1) {
    			cajas[i].setObjeto(0);
    			cajas[i].setNombreImagen("cajaUsada");
    			cambioCaja = false;
    		}
    	}
    }
    // Formas de Mario
    public void formas() {
    	if (derecha && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Derecha1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Derecha2.ordinal();
    		else
    			forma = formas.Derecha3.ordinal();
    		animacion1++;
    		if(animacion1 % 400 == 0)
    			animacion++;
    	}
    	if (izquierda && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Izquierda1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Izquierda2.ordinal();
    		else
    			forma = formas.Izquierda3.ordinal();
    		animacion1++;
    		if(animacion1 % 400 == 0)
    			animacion++;
    	}
    	if ( (!derecha && !izquierda) || (derecha && izquierda) )
    		forma = formas.Quieto.ordinal();
    	if (salto && !derecha && !izquierda)
    		forma = formas.Salto_Der.ordinal();
    		
    }
    
    // Animación final
    public void animacionFinal() {
    	if (agacharse) {
    		mario.setAltoMario(ALTO_bloques);
    		y = suelo;
    	}
    	
    	// Baja el personaje por la bandera
    	forma = formas.Salto_Der.ordinal();
        while (y <= suelo) {
        	y += 0.1;
        	dibuja();
        }
        
        // Movimiento hasta el castillo
        while (xGame != x_final + 300 ) {
            derecha = true;
            xGame += 0.1;
            formas(); 
        	dibuja();
        }
        
        // Salir del programa
        System.exit(0);
    }
}